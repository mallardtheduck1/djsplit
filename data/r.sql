SELECT COUNT(*) FROM FareGraph

SELECT fg.*, fd.DISTANCE, fg.Fare / fd.DISTANCE AS EF, ll.LAT, ll.LONG FROM FareGraph fg
INNER JOIN FLOW_DISTANCE fd ON fg.FlowID = fd.FLOW_ID
INNER JOIN StationInfo si ON fg.Origin = si.TLC
INNER JOIN NLC_LATLONG ll ON si.NLC = ll.NLC
WHERE fg.Destination = 'BHM'
ORDER BY fg.Fare / fd.DISTANCE ASC

SELECT fg.Origin, ll.LAT, ll.LONG, AVG(fg.Fare / fd.DISTANCE) AS [AVG], COUNT(fg.Destination) AS [COUNT] FROM FareGraph fg
INNER JOIN FLOW_DISTANCE fd ON fg.FlowID = fd.FLOW_ID
INNER JOIN StationInfo si ON fg.Origin = si.TLC
INNER JOIN NLC_LATLONG ll ON si.NLC = ll.NLC
WHERE fd.DISTANCE > 0
GROUP BY fg.Origin, ll.LAT, ll.LONG
ORDER BY AVG(fg.Fare / fd.DISTANCE) ASC

SELECT fg.Destination, ll.LAT, ll.LONG, AVG(fg.Fare / fd.DISTANCE) AS [AVG], COUNT(fg.Origin) AS [COUNT] FROM FareGraph fg
INNER JOIN FLOW_DISTANCE fd ON fg.FlowID = fd.FLOW_ID
INNER JOIN StationInfo si ON fg.Destination = si.TLC
INNER JOIN NLC_LATLONG ll ON si.NLC = ll.NLC
WHERE fd.DISTANCE > 0
GROUP BY fg.Destination, ll.LAT, ll.LONG
ORDER BY AVG(fg.Fare / fd.DISTANCE) ASC

SELECT DISTINCT TLC ,(
	SELECT TOP 1 fl.TOC
	FROM StationInfo si
	INNER JOIN FareGraph fg ON si.TLC = fg.Origin
	INNER JOIN FLOWS fl ON fg.FlowID = fl.FLOW_ID
	WHERE si.TLC = sio.TLC
	GROUP BY fl.TOC
	ORDER BY COUNT(TOC) DESC
) TOC, LAT, LONG
FROM StationInfo sio
INNER JOIN NLC_LATLONG ll ON sio.NLC = ll.NLC

CREATE TABLE `FareGraphSU` (
  `Origin` varchar(3) NOT NULL,
  `Destination` varchar(3) NOT NULL,
  `FlowID` varchar(7) DEFAULT NULL,
  `Type` varchar(3) DEFAULT NULL,
  `Fare` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `FareGraphSU`
  ADD KEY `Origin` (`Origin`),
  ADD KEY `Destination` (`Destination`);