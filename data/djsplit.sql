-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 02, 2019 at 04:21 PM
-- Server version: 10.1.26-MariaDB-0+deb9u1
-- PHP Version: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `djsplit`
--

-- --------------------------------------------------------

--
-- Table structure for table `CLUSTERS`
--

CREATE TABLE `CLUSTERS` (
  `CLUSTER_ID` varchar(4) DEFAULT NULL,
  `CLUSTER_NLC` varchar(4) DEFAULT NULL,
  `END_DATE` varchar(8) DEFAULT NULL,
  `START_DATE` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `FareGraphRT`
--

CREATE TABLE `FareGraphRT` (
  `Origin` varchar(3) NOT NULL,
  `Destination` varchar(3) NOT NULL,
  `FlowID` varchar(7) DEFAULT NULL,
  `Type` varchar(3) DEFAULT NULL,
  `Fare` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `FareGraphRU`
--

CREATE TABLE `FareGraphRU` (
  `Origin` varchar(3) NOT NULL,
  `Destination` varchar(3) NOT NULL,
  `FlowID` varchar(7) DEFAULT NULL,
  `Type` varchar(3) DEFAULT NULL,
  `Fare` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `FareGraphST`
--

CREATE TABLE `FareGraphST` (
  `Origin` varchar(3) NOT NULL,
  `Destination` varchar(3) NOT NULL,
  `FlowID` varchar(7) DEFAULT NULL,
  `Type` varchar(3) DEFAULT NULL,
  `Fare` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `FareGraphSU`
--

CREATE TABLE `FareGraphSU` (
  `Origin` varchar(3) NOT NULL,
  `Destination` varchar(3) NOT NULL,
  `FlowID` varchar(7) DEFAULT NULL,
  `Type` varchar(3) DEFAULT NULL,
  `Fare` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `FARES`
--

CREATE TABLE `FARES` (
  `FLOW_ID` varchar(7) NOT NULL,
  `TICKET_CODE` varchar(3) NOT NULL,
  `FARE` varchar(8) NOT NULL,
  `RESTRICTION_CODE` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `FLOWS`
--

CREATE TABLE `FLOWS` (
  `ORIGIN_CODE` varchar(4) DEFAULT NULL,
  `DESTINATION_CODE` varchar(4) DEFAULT NULL,
  `ROUTE_CODE` varchar(5) DEFAULT NULL,
  `STATUS_CODE` varchar(3) DEFAULT NULL,
  `USAGE_CODE` varchar(1) DEFAULT NULL,
  `DIRECTION` varchar(1) DEFAULT NULL,
  `END_DATE` varchar(8) DEFAULT NULL,
  `START_DATE` varchar(8) DEFAULT NULL,
  `TOC` varchar(3) DEFAULT NULL,
  `CROSS_LONDON_IND` varchar(1) DEFAULT NULL,
  `NS_DISC_IND` varchar(1) DEFAULT NULL,
  `PUBLICATION_IND` varchar(1) DEFAULT NULL,
  `FLOW_ID` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `FLOW_DISTANCE`
--

CREATE TABLE `FLOW_DISTANCE` (
  `FLOW_ID` varchar(8) NOT NULL,
  `DISTANCE` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `LOCATIONS`
--

CREATE TABLE `LOCATIONS` (
  `UIC_CODE` varchar(7) DEFAULT NULL,
  `END_DATE` varchar(8) DEFAULT NULL,
  `START_DATE` varchar(8) DEFAULT NULL,
  `QUOTE_DATE` varchar(8) DEFAULT NULL,
  `ADMIN_AREA_CODE` varchar(3) DEFAULT NULL,
  `NLC_CODE` varchar(4) DEFAULT NULL,
  `DESCRIPTION` varchar(16) DEFAULT NULL,
  `CRS_CODE` varchar(3) DEFAULT NULL,
  `RESV_CODE` varchar(5) DEFAULT NULL,
  `ERS_COUNTRY` varchar(2) DEFAULT NULL,
  `ERS_CODE` varchar(3) DEFAULT NULL,
  `FARES_GROUP` varchar(6) DEFAULT NULL,
  `COUNTY` varchar(2) DEFAULT NULL,
  `PTE_CODE` varchar(2) DEFAULT NULL,
  `ZONE_NO` varchar(4) DEFAULT NULL,
  `ZONE_IND` varchar(2) DEFAULT NULL,
  `REGION` varchar(1) DEFAULT NULL,
  `HIERARCHY` varchar(1) DEFAULT NULL,
  `CC_DESC_OUT` varchar(41) DEFAULT NULL,
  `CC_DESC_RTN` varchar(16) DEFAULT NULL,
  `ATB_DESC_OUT` varchar(60) DEFAULT NULL,
  `ATB_DESC_RTN` varchar(30) DEFAULT NULL,
  `SPECIAL_FACILITIES` varchar(26) DEFAULT NULL,
  `LUL_DIRECTION_IND` varchar(1) DEFAULT NULL,
  `LUL_UTS_MODE` varchar(1) DEFAULT NULL,
  `LUL_ZONE_1` varchar(1) DEFAULT NULL,
  `LUL_ZONE_2` varchar(1) DEFAULT NULL,
  `LUL_ZONE_3` varchar(1) DEFAULT NULL,
  `LUL_ZONE_4` varchar(1) DEFAULT NULL,
  `LUL_ZONE_5` varchar(1) DEFAULT NULL,
  `LUL_ZONE_6` varchar(1) DEFAULT NULL,
  `LUL_UTS_LONDON_STN` varchar(1) DEFAULT NULL,
  `UTS_CODE` varchar(3) DEFAULT NULL,
  `UTS_A_CODE` varchar(3) DEFAULT NULL,
  `UTS_PTR_BIAS` varchar(1) DEFAULT NULL,
  `UTS_OFFSET` varchar(1) DEFAULT NULL,
  `UTS_NORTH` varchar(3) DEFAULT NULL,
  `UTS_EAST` varchar(3) DEFAULT NULL,
  `UTS_SOUTH` varchar(3) DEFAULT NULL,
  `UTS_WEST` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `MinFaresRT`
--

CREATE TABLE `MinFaresRT` (
  `Destination` varchar(3) NOT NULL,
  `MinFare` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `MinFaresRU`
--

CREATE TABLE `MinFaresRU` (
  `Destination` varchar(3) NOT NULL,
  `MinFare` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `MinFaresST`
--

CREATE TABLE `MinFaresST` (
  `Destination` varchar(3) NOT NULL,
  `MinFare` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `MinFaresSU`
--

CREATE TABLE `MinFaresSU` (
  `Destination` varchar(3) NOT NULL,
  `MinFare` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `NDFARES`
--

CREATE TABLE `NDFARES` (
  `ORIGIN_CODE` varchar(4) DEFAULT NULL,
  `DESTINATION_CODE` varchar(4) DEFAULT NULL,
  `ROUTE_CODE` varchar(5) DEFAULT NULL,
  `RAILCARD_CODE` varchar(3) DEFAULT NULL,
  `TICKET_CODE` varchar(3) DEFAULT NULL,
  `ND_RECORD_TYPE` varchar(1) DEFAULT NULL,
  `END_DATE` varchar(8) DEFAULT NULL,
  `START_DATE` varchar(8) DEFAULT NULL,
  `QUOTE_DATE` varchar(8) DEFAULT NULL,
  `SURPRESS_MKR` varchar(1) DEFAULT NULL,
  `ADULT_FARE` varchar(8) DEFAULT NULL,
  `CHILD_FARE` varchar(8) DEFAULT NULL,
  `RESTRICTION_CODE` varchar(2) DEFAULT NULL,
  `COMPOSITE_INDICATOR` varchar(1) DEFAULT NULL,
  `CROSS_LONDON_IND` varchar(1) DEFAULT NULL,
  `PS_IND` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `NLC_DISTANCE`
--

CREATE TABLE `NLC_DISTANCE` (
  `NLC1` varchar(4) DEFAULT NULL,
  `NLC2` varchar(4) DEFAULT NULL,
  `DISTANCE` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `NLC_LATLONG`
--

CREATE TABLE `NLC_LATLONG` (
  `NLC` varchar(4) NOT NULL,
  `LAT` float DEFAULT NULL,
  `LONG` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `RESTRICTIONS`
--

CREATE TABLE `RESTRICTIONS` (
  `RESTRICTION_CODE` varchar(2) NOT NULL,
  `DESCRIPTION` varchar(30) NOT NULL,
  `DESC_OUT` varchar(50) NOT NULL,
  `DESC_RTN` varchar(50) NOT NULL,
  `TYPE_OUT` varchar(1) NOT NULL,
  `TYPE_RTN` varchar(1) NOT NULL,
  `CHANGE_IND` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ROUTES`
--

CREATE TABLE `ROUTES` (
  `ROUTE_CODE` varchar(5) NOT NULL,
  `END_DATE` varchar(8) NOT NULL,
  `START_DATE` varchar(8) NOT NULL,
  `QUOTE_DATE` varchar(8) NOT NULL,
  `DESCRIPTION` varchar(16) NOT NULL,
  `ATB_DESC_1` varchar(35) NOT NULL,
  `ATB_DESC_2` varchar(35) NOT NULL,
  `ATB_DESC_3` varchar(35) NOT NULL,
  `ATB_DESC_4` varchar(35) NOT NULL,
  `CC_DESC` varchar(16) NOT NULL,
  `AAA_DESC` varchar(41) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `StationInfo`
--

CREATE TABLE `StationInfo` (
  `NLC` varchar(4) DEFAULT NULL,
  `TLC` varchar(3) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Region` varchar(50) DEFAULT NULL,
  `Authority` varchar(50) DEFAULT NULL,
  `Constituency` varchar(50) DEFAULT NULL,
  `OSEasting` varchar(10) DEFAULT NULL,
  `OSNorthing` varchar(10) DEFAULT NULL,
  `SFO` varchar(50) DEFAULT NULL,
  `Group` varchar(50) DEFAULT NULL,
  `PTE` varchar(50) DEFAULT NULL,
  `LTA` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `CLUSTERS`
--
ALTER TABLE `CLUSTERS`
  ADD KEY `CLUSTER_NLC` (`CLUSTER_NLC`),
  ADD KEY `CLUSTER_ID` (`CLUSTER_ID`);

--
-- Indexes for table `FareGraphRT`
--
ALTER TABLE `FareGraphRT`
  ADD PRIMARY KEY (`Origin`,`Destination`),
  ADD KEY `Origin` (`Origin`),
  ADD KEY `Destination` (`Destination`),
  ADD KEY `Fare` (`Fare`),
  ADD KEY `FlowID` (`FlowID`);

--
-- Indexes for table `FareGraphRU`
--
ALTER TABLE `FareGraphRU`
  ADD PRIMARY KEY (`Origin`,`Destination`),
  ADD KEY `Origin` (`Origin`),
  ADD KEY `Destination` (`Destination`),
  ADD KEY `Fare` (`Fare`),
  ADD KEY `FlowID` (`FlowID`);

--
-- Indexes for table `FareGraphST`
--
ALTER TABLE `FareGraphST`
  ADD PRIMARY KEY (`Origin`,`Destination`),
  ADD KEY `Origin` (`Origin`),
  ADD KEY `Destination` (`Destination`),
  ADD KEY `Fare` (`Fare`),
  ADD KEY `FlowID` (`FlowID`);

--
-- Indexes for table `FareGraphSU`
--
ALTER TABLE `FareGraphSU`
  ADD PRIMARY KEY (`Origin`,`Destination`),
  ADD KEY `Origin` (`Origin`),
  ADD KEY `Destination` (`Destination`),
  ADD KEY `Fare` (`Fare`),
  ADD KEY `FlowID` (`FlowID`);

--
-- Indexes for table `FARES`
--
ALTER TABLE `FARES`
  ADD PRIMARY KEY (`FLOW_ID`,`TICKET_CODE`,`FARE`,`RESTRICTION_CODE`),
  ADD KEY `FLOW_ID` (`FLOW_ID`),
  ADD KEY `TICKET_CODE` (`TICKET_CODE`);

--
-- Indexes for table `FLOWS`
--
ALTER TABLE `FLOWS`
  ADD PRIMARY KEY (`FLOW_ID`),
  ADD KEY `ORIGIN_CODE` (`ORIGIN_CODE`),
  ADD KEY `DESTINATION_CODE` (`DESTINATION_CODE`);

--
-- Indexes for table `FLOW_DISTANCE`
--
ALTER TABLE `FLOW_DISTANCE`
  ADD PRIMARY KEY (`FLOW_ID`);

--
-- Indexes for table `LOCATIONS`
--
ALTER TABLE `LOCATIONS`
  ADD KEY `CRS_CODE` (`CRS_CODE`),
  ADD KEY `FARES_GROUP` (`FARES_GROUP`);

--
-- Indexes for table `MinFaresRT`
--
ALTER TABLE `MinFaresRT`
  ADD PRIMARY KEY (`Destination`);

--
-- Indexes for table `MinFaresSU`
--
ALTER TABLE `MinFaresSU`
  ADD PRIMARY KEY (`Destination`);

--
-- Indexes for table `NLC_DISTANCE`
--
ALTER TABLE `NLC_DISTANCE`
  ADD KEY `NLC1` (`NLC1`,`NLC2`);

--
-- Indexes for table `NLC_LATLONG`
--
ALTER TABLE `NLC_LATLONG`
  ADD PRIMARY KEY (`NLC`);

--
-- Indexes for table `RESTRICTIONS`
--
ALTER TABLE `RESTRICTIONS`
  ADD KEY `RESTRICTION_CODE` (`RESTRICTION_CODE`);

--
-- Indexes for table `ROUTES`
--
ALTER TABLE `ROUTES`
  ADD KEY `ROUTE_CODE` (`ROUTE_CODE`);

--
-- Indexes for table `StationInfo`
--
ALTER TABLE `StationInfo`
  ADD UNIQUE KEY `TLC` (`TLC`),
  ADD KEY `NLC` (`NLC`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
