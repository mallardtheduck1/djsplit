#!/usr/bin/python

import MySQLdb
import csv
from UserString import MutableString
from dbconfig import getDbWriteLogin

dbLogin = getDbWriteLogin()

cnxn = MySQLdb.connect(host="localhost",
                      user=dbLogin[0],
                      passwd=dbLogin[1],
                      db="djsplit")

with open('../data/StationInfo.csv', 'rb') as csvfile:
	reader = csv.DictReader(csvfile)
	count = 0
	for row in reader:
		count += 1
		sql = MutableString()
		sql += 'INSERT INTO `StationInfo` (`NLC`,`TLC`,`Name`,`Region`,`Authority`,`Constituency`,`OSEasting`,`OSNorthing`,`SFO`,`Group`,`PTE`,`LTA`) VALUES ('
		sql += "'" + row['NLC'].replace('\'', '\'\'') + "',"
		sql += "'" + row['TLC'].replace('\'', '\'\'') + "',"
		sql += "'" + row['Station Name'].replace('\'', '\'\'') + "',"
		sql += "'" + row['Region'].replace('\'', '\'\'') + "',"
		sql += "'" + row['Local Authority'].replace('\'', '\'\'') + "',"
		sql += "'" + row['Constituency'].replace('\'', '\'\'') + "',"
		sql += "'" + row['OS Grid Easting'].replace('\'', '\'\'') + "',"
		sql += "'" + row['OS Grid Northing'].replace('\'', '\'\'') + "',"
		sql += "'" + row['Station Facility Owner'].replace('\'', '\'\'') + "',"
		sql += "'" + row['Station Group'].replace('\'', '\'\'') + "',"
		sql += "'" + row['PTE Urban Area Station'].replace('\'', '\'\'') + "',"
		sql += "'" + row['London Travelcard Area'].replace('\'', '\'\'') + "')"
		cursor = cnxn.cursor()
		cursor.execute(str(sql))
		cnxn.commit()
		print count, row['Station Name']
