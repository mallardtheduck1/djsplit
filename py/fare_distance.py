#!/usr/bin/python

import MySQLdb
import gpxpy.geo
import pylru
from dbconfig import getDbWriteLogin

dbLogin = getDbWriteLogin()

cache_size = 32768

def get_cnxn():
	return MySQLdb.connect(host="localhost",
                      user=dbLogin[0],
                      passwd=dbLogin[1],
                      db="djsplit")

glcnxn = get_cnxn()
glcache = pylru.lrucache(cache_size)
def get_latlong(nlc):
	if nlc in glcache: return glcache[nlc]
	distcur = glcnxn.cursor()
	distcur.execute("SELECT `LAT`, `LONG` FROM NLC_LATLONG WHERE NLC = '%s'" % nlc)
	for row in distcur:
		lat = float(row[0])
		long = float(row[1])
		if lat == -999.0: lat = None
		if long == -999.0: long = None
		latlong = [lat, long]
		glcache[nlc] = latlong
		return latlong
	distcur.execute("SELECT `LAT`, `LONG` FROM NLC_LATLONG ll INNER JOIN CLUSTERS c ON ll.NLC = c.CLUSTER_NLC WHERE c.CLUSTER_ID = '%s'" % nlc)
	for row in distcur:
		lat = float(row[0])
		long = float(row[1])
		latlong = [lat, long]
		distcur.execute("INSERT INTO NLC_LATLONG (`NLC`, `LAT`, `LONG`) VALUES ('%s', %f, %f)" % (nlc, lat, long))
		glcnxn.commit()
		glcache[nlc] = latlong
		return latlong
	distcur.execute("INSERT INTO NLC_LATLONG (`NLC`, `LAT`, `LONG`) VALUES ('%s', -999.0, -999.0)" % nlc)
	glcnxn.commit()
	glcache[nlc] = [None, None]
	return None, None

def calc_dist(nlc1, nlc2):
	lat1, long1 = get_latlong(nlc1)
	lat2, long2 = get_latlong(nlc2)
	if lat1 is not None and lat2 is not None: 
		return gpxpy.geo.haversine_distance(lat1, long1, lat2, long2) / 1000.0
	return None
	
nlcnxn = get_cnxn()
nlcache = pylru.lrucache(cache_size)
def nlc_distance(nlc1, nlc2):
	key = "%s:%s" % (nlc1, nlc2)
	if key in nlcache: return nlcache[key]
	if nlc1 == nlc2: return 0.0
	nlccur = nlcnxn.cursor()
	nlccur.execute("SELECT DISTANCE FROM NLC_DISTANCE WHERE NLC1 = '%s' AND NLC2 = '%s'" % (nlc1, nlc2))
	for row in nlccur:
		dist = float(row[0])
		nlcache[key] = dist
		return dist
	dist = calc_dist(nlc1, nlc2)
	if dist is not None:
		nlccur.execute("INSERT INTO NLC_DISTANCE (NLC1, NLC2, DISTANCE) VALUES ('%s', '%s', %f)" % (nlc1, nlc2, dist))
		nlcnxn.commit()
	nlcache[key] = dist
	return dist

flcnxn = get_cnxn()
flcache = pylru.lrucache(cache_size)
def flow_distance(flow):
	if flow in flcache: return flcache[flow]
	flcur = flcnxn.cursor()
	flcur.execute("SELECT DISTANCE FROM FLOW_DISTANCE WHERE FLOW_ID = '%s'" % flow)
	for row in flcur:
		dist = float(row[0])
		flcache[flow] = dist
		return dist
	flcur.execute("SELECT ORIGIN_CODE, DESTINATION_CODE FROM FLOWS WHERE FLOW_ID = '%s'" % flow)
	for row in flcur:
		onlc = row[0]
		dnlc = row[1]
		snlc = [onlc, dnlc]
		snlc.sort()
		nlc1, nlc2 = snlc
		dist = nlc_distance(nlc1, nlc2)
		if dist is not None:
			flcur.execute("INSERT INTO FLOW_DISTANCE (FLOW_ID, DISTANCE) VALUES ('%s', %f)" % (flow, dist))
			flcnxn.commit()
		flcache[flow] = dist
		return dist
	flcache[flow] = None
	return None

cnxn = get_cnxn()
rows = 1
while rows > 0:
	rows = 0
	cursor = cnxn.cursor()
	cursor.execute(
	"SELECT f.FLOW_ID, TICKET_CODE, FARE FROM FARES f "
	"LEFT JOIN FLOW_DISTANCE df ON f.FLOW_ID = df.FLOW_ID "
	"INNER JOIN FLOWS fl ON f.FLOW_ID = fl.FLOW_ID "
	"LEFT JOIN NLC_LATLONG oloc ON fl.ORIGIN_CODE = oloc.NLC "
	"LEFT JOIN NLC_LATLONG dloc ON fl.DESTINATION_CODE = dloc.NLC "
	"WHERE f.TICKET_CODE IN ('SOS', 'SDS', 'SVS', 'CDS') "
	"AND df.DISTANCE IS NULL "
	"AND COALESCE(oloc.LAT, -1) != -999.0 AND COALESCE(dloc.LAT, -1) != -999.0 "
	"LIMIT 1000"
	)
	for row in cursor:
		flow = row[0]
		type = row[1]
		fare = int(row[2])
		dist = flow_distance(flow)
		effi = -1.0
		if dist is not None:
			if dist > 0: effi = fare/dist
		else:
			dist = -1.0
		print "Flow: %s Type: %s Fare: %d Distance: %f Fare/Distance: %f" % (flow, type, fare, dist, effi)
		rows += 1
	cnxn.commit()
	cursor.close()
