#!/usr/bin/python

import MySQLdb
import MySQLdb.cursors
import pylru
import itertools
from dbconfig import getDbWriteLogin

dbLogin = getDbWriteLogin()

cache_size = 3000
check_existence = False

changeCount = 0

cnxn = MySQLdb.connect(host="localhost",
                      user=dbLogin[0],
                      passwd=dbLogin[1],
                      db="djsplit",
                      cursorclass = MySQLdb.cursors.SSCursor)
                      
fareTableConfig = [
	{
		'tableName': 'FareGraphST',
		'fareTypes': ['SOS', 'SDS', 'SVS', 'CDS']
	},
	{
		'tableName': 'FareGraphSU',
		'fareTypes': ['SOS', 'SDS', 'SVS', 'CDS', 'OPS', 'SSS']
	},
	{
		'tableName': 'FareGraphRT',
		'fareTypes': ['SOR', 'SVR', 'SHR', '~FR'],
		'singlesTable' : 'FareGraphST'
	},
	{
		'tableName': 'FareGraphRU',
		'fareTypes': ['SOR', 'SVR', 'SHR', 'SSR', 'OPR', '~FR'],
		'singlesTable' : 'FareGraphSU'
	}
]

def getAllTicketTypes():
	ret = []
	for c in fareTableConfig:
		ret += c['fareTypes']
	return list(set(ret))
	
def toSQLList(a):
	s = ', '.join(['\'%s\''] * len(a))
	return '(' + (s % tuple(a)) + ')'

tlcache = pylru.lrucache(cache_size)			  
def tlcToNlc(tlc):
	if tlc in tlcache: return tlcache[tlc]
	cursor = cnxn.cursor()
	cursor.execute('SELECT FARES_GROUP, NLC_CODE FROM LOCATIONS WHERE CRS_CODE = \'' + tlc + '\'')
	ret = []
	for row in cursor:
		nlc = row[0].strip()
		fg = row[1].strip()
		if(nlc not in ret): ret.append(nlc)
		if(fg not in ret): ret.append(fg)
		ret.append(row[0])
	tlcache[tlc] = ret
	return ret

locache = pylru.lrucache(cache_size)		
def getLocs(tlc):
	if tlc in locache: return locache[tlc]
	locs = tlcToNlc(tlc)
	cursor = cnxn.cursor()
	ret = []
	for loc in locs:
		loc = loc.strip()
		if(loc not in ret):
			ret.append(loc)
			cursor.execute('SELECT CLUSTER_ID FROM CLUSTERS WHERE CLUSTER_NLC = \'' + loc + '\'')
			for row in cursor:
				nlc = row[0].strip()
				if(nlc not in ret): ret.append(nlc)
	locache[tlc] = ret
	return ret
	
def getAllFares(olocs, dlocs):
	if len(olocs) == 0 or len(dlocs) == 0: return []
	sql = 'SELECT FLOW_ID FROM FLOWS WHERE (ORIGIN_CODE IN ('
	sql += ', '.join(["'{}'".format(o) for o in olocs])
	sql += ') AND DESTINATION_CODE IN ('
	sql += ', '.join(["'{}'".format(d) for d in dlocs])
	sql += ')) OR (DESTINATION_CODE IN ('
	sql += ', '.join(["'{}'".format(o) for o in olocs])
	sql += ') AND ORIGIN_CODE IN ('
	sql += ', '.join(["'{}'".format(d) for d in dlocs])
	sql += ') AND DIRECTION = \'R\')'
	cursor = cnxn.cursor()
	cursor.execute(sql)
	flows = []
	for row in cursor:
		flows.append(row[0])
	ret = []
	for f in flows:
		sql = 'SELECT FLOW_ID, TICKET_CODE, FARE, RESTRICTION_CODE FROM FARES WHERE FLOW_ID = \'' + f + '\' AND TICKET_CODE IN ' + toSQLList(getAllTicketTypes());
		cursor.execute(sql)
		for row in cursor:
			ret.append(row)
	return ret

def getFakeReturn(origin, destination, singlesTable):
	sql = "SELECT Fare, FlowID FROM %s WHERE (Origin = '%s' AND Destination = '%s') OR (Origin = '%s' AND Destination = '%s')" % (singlesTable, origin, destination, destination, origin)
	cursor = cnxn.cursor()
	cursor.execute(sql)
	fare = 0
	flow = ''
	count = 0
	for row in cursor:
		fare += int(row[0])
		flow = row[1]
		count += 1
	ret = (flow, '~FR', fare, '??')
	if count == 2: return ret
	else: return None

def getLowestFares(origin, destination):
	olocs = getLocs(origin)
	dlocs = getLocs(destination)
	fares = getAllFares(olocs, dlocs)
	
	efares = {}
	
	for c in fareTableConfig:
		cfares = fares
		if 'singlesTable' in c and c['singlesTable'] in efares:
			sfare = efares[c['singlesTable']]
			if sfare is not None: fares.append((sfare[0], '~FR', int(sfare[2]) * 2, '??'))
		
		minfare = 999999999
		minrow = None
		for f in cfares:
			if f[1] in c['fareTypes'] and int(f[2]) < minfare:
				minfare = int(f[2])
				minrow = f
		if minrow is not None:
			print "%s -> %s (%s) %s: %i" % (origin, destination, c['tableName'], minrow[1], int(minrow[2]))
			efares[c['tableName']] = minrow
			if not check_existence or not fareExists(origin, destination, c['tableName']):
				addEdge(origin, destination, minrow, c['tableName'])

def addEdge(origin, destination, fare, tableName):
	global changeCount
	cursor = cnxn.cursor()
	cursor.execute("INSERT INTO %s (Origin, Destination, FlowID, Type, Fare) VALUES ('%s', '%s', '%s', '%s', %d)" % (tableName, origin, destination, fare[0], fare[1], int(fare[2])))
	changeCount += 1

def getPairs():
	cursor = cnxn.cursor()
	cursor.execute("SELECT s1.TLC, s2.TLC FROM StationInfo s1 CROSS JOIN StationInfo s2 WHERE s1.TLC != s2.TLC")
	ret = []
	for row in cursor:
		ret.append([row[0], row[1]])
	return ret
	
def getAllTlcs():
	cursor = cnxn.cursor()
	cursor.execute("SELECT TLC FROM StationInfo")
	ret = []
	for row in cursor:
		ret.append(row[0])
	return ret

extCache = pylru.lrucache(cache_size)
def getExisting(origin, tableName):
	if origin in extCache: return extCache[origin]
	cursor = cnxn.cursor()
	cursor.execute("SELECT Destination FROM %s WHERE Origin = '%s'" % (tableName, origin))
	ret = set()
	for row in cursor:
		ret.add(row[0])
	extCache[origin] = ret
	return ret

def fareExists(origin, destination, tableName):
	ext = getExisting(origin, tableName)
	if destination in ext: return True
	else: return False

#pairs = getPairs()
#for p in pairs:
#	f = getLowestFare(p[0], p[1])
#	if f is not None: 
#		print "%s -> %s: " % (p[0], p[1]), f
#		addEdge(p[0], p[1], f)
	
tlcs = getAllTlcs()
changeCount = 0
for p in itertools.combinations(tlcs, 2):
	getLowestFares(p[0], p[1])
	getLowestFares(p[1], p[0])
	if changeCount > 99: 
		cnxn.commit()
		changeCount = 0
if changeCount > 0: cnxn.commit()
