#!/usr/bin/python

import MySQLdb
import struct
from dbconfig import getDbWriteLogin

dbLogin = getDbWriteLogin()

cnxn = MySQLdb.connect(host="localhost",
                      user=dbLogin[0],
                      passwd=dbLogin[1],
                      db="djsplit")
					  
def getstructparser(fieldwidths):
	fmtstring = ' '.join('{}{}'.format(abs(fw), 'x' if fw < 0 else 's')
                        for fw in fieldwidths)
	return struct.Struct(fmtstring).unpack_from
	

restriction_parse = getstructparser((-4, 2, 30, 50, 50, 1, 1, 1))
def process_restriction(line):
	global restriction_parse
	fields = restriction_parse(line)
	from UserString import MutableString
	sql = MutableString()
	sql += 'INSERT INTO RESTRICTIONS (`RESTRICTION_CODE`,`DESCRIPTION`,`DESC_OUT`,`DESC_RTN`, `TYPE_OUT`, `TYPE_RTN`, `CHANGE_IND`) VALUES '
	sql += '('
	sql += ', '.join(["'{}'".format(f) for f in fields])
	sql += ')'
	cursor = cnxn.cursor()
	cursor.execute(str(sql))
	cnxn.commit()

file = open("../data/RJFAF017.RST")

count = 0
for line in file:
	count += 1
	print count, line
	type = line[:3]
	if type == 'RRH':
		process_restriction(line)
