#!/usr/bin/python

import MySQLdb
import struct
from dbconfig import getDbWriteLogin

dbLogin = getDbWriteLogin()

cnxn = MySQLdb.connect(host="localhost",
                      user=dbLogin[0],
                      passwd=dbLogin[1],
                      db="djsplit")
	  
def getstructparser(fieldwidths):
	fmtstring = ' '.join('{}{}'.format(abs(fw), 'x' if fw < 0 else 's')
                        for fw in fieldwidths)
	return struct.Struct(fmtstring).unpack_from
	

loc_parse = getstructparser((-2, 7, 8, 8, 8, 3, 4, 16, 3, 5, 2, 3, 6, 2, 2, 4, 2, 1, 1, 41, 16, 60, 30, 26, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 3, 1, 1, 3, 3, 3, 3))
def process_loc(line):
	global loc_parse
	fields = loc_parse(line)
	from UserString import MutableString
	sql = MutableString()
	sql += 'INSERT INTO LOCATIONS (`UIC_CODE`,`END_DATE`,`START_DATE`,`QUOTE_DATE`,`ADMIN_AREA_CODE`,`NLC_CODE`,`DESCRIPTION`,`CRS_CODE`,`RESV_CODE`,`ERS_COUNTRY`,`ERS_CODE`,`FARES_GROUP`,`COUNTY`,`PTE_CODE`,`ZONE_NO`,`ZONE_IND`,`REGION`,`HIERARCHY`,`CC_DESC_OUT`,`CC_DESC_RTN`,`ATB_DESC_OUT`,`ATB_DESC_RTN`,`SPECIAL_FACILITIES`,`LUL_DIRECTION_IND`,`LUL_UTS_MODE`,`LUL_ZONE_1`,`LUL_ZONE_2`,`LUL_ZONE_3`,`LUL_ZONE_4`,`LUL_ZONE_5`,`LUL_ZONE_6`,`LUL_UTS_LONDON_STN`,`UTS_CODE`,`UTS_A_CODE`,`UTS_PTR_BIAS`,`UTS_OFFSET`,`UTS_NORTH`,`UTS_EAST`,`UTS_SOUTH`,`UTS_WEST`) VALUES '
	sql += '('
	sql += ', '.join(["'{}'".format(f.replace('\'', '\'\'')) for f in fields])
	sql += ')'
	cursor = cnxn.cursor()
	cursor.execute(str(sql))
	cnxn.commit()

file = open("../data/RJFAF017.LOC")

count = 0
for line in file:
	count += 1
	print count, line
	type = line[1]
	if type == 'L':
		process_loc(line)
