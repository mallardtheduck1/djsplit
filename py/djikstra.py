#!/usr/bin/python
# encoding=utf8

from __future__ import unicode_literals

import MySQLdb
import MySQLdb.cursors
import sys
import itertools
import collections
from heap import MinHeap
from Queue import PriorityQueue
import pylru
from dbconfig import getDbPassword

reload(sys)
sys.setdefaultencoding('utf-8')

cnxn = MySQLdb.connect(host="localhost",
                      user="djsplit",
                      passwd=getDbPassword(),
                      db="djsplit",
                      cursorclass = MySQLdb.cursors.SSCursor)

tableName = ''
singleTable = ''
minFareTable = ''
minFare = 7

queryCount = 0

def incQueryCount():
	global queryCount
	queryCount += 1

def setupTables(code):
	global tableName
	global singleTable
	global minFareTable
	
	if code == 'ST':
		tableName = 'FareGraphST'
		minFareTable = 'MinFaresST'
		return
	if code == 'SU':
		tableName = 'FareGraphSU'
		minFareTable = 'MinFaresSU'
		return
	if code == 'RT':
		tableName = 'FareGraphRT'
		minFareTable = 'MinFaresRT'
		singleTable = 'FareGraphST'
		return
	if code == 'RU':
		tableName = 'FareGraphRU'
		minFareTable = 'MinFaresRU'
		singleTable = 'FareGraphSU'
		return
	print 'Unknown dataset:', code
	sys.exit(-1)
	
def setupMinFare(dest):
	global minFare
	cursor = cnxn.cursor()
	cursor.execute("SELECT MinFare FROM %s WHERE Destination = '%s'" % (minFareTable, dest))
	incQueryCount()
	for row in cursor:
		minFare = row[0]
		return
                      
def pairwise(iterable):
    a, b = itertools.tee(iterable)
    next(b, None)
    return itertools.izip(a, b)
	
def asMoney(v):
	if v < 100: return "%dp" % v
	sv = str(v)
	pence = sv[-2:]
	pounds = sv[:-2]
	return "£%s.%s"  % (pounds, pence)

class Edge:
	def __init__(self):
		self.origin = ""
		self.destination = ""
		self.fare = 0
		self.flow = ""
		self.type = ""
		
	def __hash__(self):
		return hash((self.origin, self.destination))
		
	def __eq__(self, other):
		return other and self.origin == other.origin and self.destination == other.destination

	def __ne__(self, other):
		return not self.__eq__(other)
		
	def __str__(self):
		return (u"%s (%s) -> %s (%s) %s %s" % 
		(getStationName(self.origin), self.origin, getStationName(self.destination), self.destination, self.type, asMoney(self.fare)))
		
	def __unicode__(self):
		return str(self)

def make_edge(row):
	e = Edge()
	e.origin = row[0]
	e.destination = row[1]
	e.fare = int(row[2])
	e.flow = row[3]
	e.type = row[4]
	return e

def getEdges(node, maxFare):
	cursor = cnxn.cursor()
	cursor.execute(
		"SELECT Origin, Destination, Fare, FlowID, Type FROM %s WHERE Origin = '%s' AND Fare < %i" 
		% (tableName, node, maxFare)
	)
	incQueryCount()
	ret = []
	for row in cursor:
		ret.append(make_edge(row))
	return ret

def getDirect(origin, destination, theTable = ''):
	if theTable == '': theTable = tableName
	cursor = cnxn.cursor()
	cursor.execute("SELECT Origin, Destination, Fare, FlowID, Type FROM %s WHERE Origin = '%s' AND Destination = '%s'" % (theTable, origin, destination))
	incQueryCount()
	for row in cursor:
		return make_edge(row)
	return None

def nlcToTlcs(nlc):
	cursor = cnxn.cursor()
	ret = []
	cursor.execute("SELECT TLC FROM StationInfo WHERE NLC = '%s'" % nlc)
	incQueryCount()
	for row in cursor:
		ret.append(row[0])
	cursor.execute("SELECT si.TLC FROM CLUSTERS cl INNER JOIN StationInfo si ON cl.CLUSTER_NLC = si.NLC WHERE CLUSTER_ID = '%s'" % nlc)
	incQueryCount()
	for row in cursor:
		ret.append(row[0])
	cursor.execute("SELECT CRS_CODE FROM LOCATIONS WHERE FARES_GROUP = '%s'" % nlc)
	incQueryCount()
	for row in cursor:
		ret.append(row[0])
	pret = []
	for r in ret:
		pr = r.strip()
		if pr != '': pret.append(pr)
	return list(set(pret))

def getOriginStations(flow):
	cursor = cnxn.cursor()
	cursor.execute("SELECT ORIGIN_CODE FROM FLOWS WHERE FLOW_ID = '%s'" % flow)
	incQueryCount()
	nlc = None
	for row in cursor:
		nlc = row[0]
	if nlc is None: return None
	return nlcToTlcs(nlc)

def getDestinationStations(flow):
	cursor = cnxn.cursor()
	cursor.execute("SELECT DESTINATION_CODE FROM FLOWS WHERE FLOW_ID = '%s'" % flow)
	incQueryCount()
	nlc = None
	for row in cursor:
		nlc = row[0]
	if nlc is None: return None
	return nlcToTlcs(nlc)
	
def getNodeStations(flow, station):
	os = getOriginStations(flow)
	if station in os: return os
	ds = getDestinationStations(flow)
	if station in ds: return ds
	return None

dstCache = pylru.lrucache(6000)
def guesstimateCost(origin, destination):
	global dstCache
	key = '%s:%s' % (origin, destination)
	if key in dstCache: return dstCache[key]
	#print "%s not in cache!" % key
	cursor = cnxn.cursor()
	cursor.execute(
		"SELECT fd.DISTANCE FROM %s fg "
		"INNER JOIN FLOW_DISTANCE fd ON fg.FlowID = fd.FLOW_ID "
		"WHERE fg.Origin = '%s' AND fg.Destination = '%s'" % (tableName, origin, destination))
	incQueryCount()
	for row in cursor:
		guess = row[0] * minFare
		dstCache[key] = guess
		return guess
	dstCache[key] = 0
	return 0
	
def guesstimateCosts(destination):
	global dstCache
	cursor = cnxn.cursor()
	cursor.execute(
		"SELECT fg.Origin, fd.DISTANCE FROM %s fg "
		"INNER JOIN FLOW_DISTANCE fd ON fg.FlowID = fd.FLOW_ID "
		"WHERE fg.Destination = '%s'" % (tableName, destination))
	incQueryCount()
	ret = []
	for row in cursor:
		guess = row[1] * minFare
		ret.append((row[0], guess))
		key = '%s:%s' % (row[0], destination)
		dstCache[key] = guess
	ret.append((destination, 0))
	key = '%s:%s' % (destination, destination)
	dstCache[key] = 0
	return ret

def findRoute(origin, destination, directFare):
	frontier = PriorityQueue()
	frontier.put((0, origin))
	came_from = {}
	cost_so_far = {}
	came_from[origin] = None
	cost_so_far[origin] = 0
	visited = []
	minFound = directFare
	guesstimateCosts(destination)
	
	while not frontier.empty():
		(p, current) = frontier.get()
		if cost_so_far[current] > minFound: continue
		if current in visited: continue
		
		if current == destination:
			route = []
			while current in came_from.keys():
				route.append(current)
				current = came_from[current]
			return list(reversed(route))
	
		costRemaining = minFound - cost_so_far[current]
		edges = getEdges(current, costRemaining)
		for edge in edges:
			next = edge.destination
			if edge.fare > costRemaining: continue
			nextGuess = guesstimateCost(next, destination)
			if nextGuess + edge.fare > costRemaining: continue
			new_cost = cost_so_far[current] + edge.fare
			if next == destination and new_cost < minFound: minFound = new_cost
			if next not in cost_so_far or new_cost < cost_so_far[next]:
				cost_so_far[next] = new_cost
				guess = guesstimateCost(next, destination)
				if guess is None: guess = 0
				priority = new_cost + guess
				frontier.put((priority, next))
				came_from[next] = current
		visited.append(current)
	return []

def getRestriction(flow, rtn = False):
	cursor = cnxn.cursor()
	cursor.execute(
		"SELECT `DESCRIPTION`, `DESC_OUT`, `DESC_RTN`, R.`RESTRICTION_CODE` FROM `RESTRICTIONS` R "
		"INNER JOIN `FARES` F ON F.`RESTRICTION_CODE` = R.`RESTRICTION_CODE` "
		"INNER JOIN %s FG ON F.`FLOW_ID` = FG.`FlowID` AND F.`TICKET_CODE` = FG.`Type` "
		"WHERE FG.`FlowID` = '%s'" % (tableName, flow))
	incQueryCount()
	for row in cursor:
		description = row[0].strip()
		desc_out = row[1].strip()
		desc_rtn = row[2].strip()
		code = row[3].strip()
		ret = code + ': ' + description + ' OUT: ' + desc_out
		if(rtn): ret += ' RTN: ' + desc_rtn
		return ret
	return ''
	
def getRouteDesc(flow):
	cursor = cnxn.cursor()
	cursor.execute(
		"SELECT R.`ATB_DESC_1`, R.`ATB_DESC_2`, R.`ATB_DESC_3`, R.`ATB_DESC_4`, R.`ROUTE_CODE`, R.`DESCRIPTION` FROM `ROUTES` R "
		"INNER JOIN `FLOWS` F ON F.`ROUTE_CODE` = R.`ROUTE_CODE` "
		"WHERE F.`FLOW_ID` = '%s'" % (flow))
	incQueryCount()
	for row in cursor:
		atb1 = row[0].strip()
		atb2 = row[1].strip()
		atb3 = row[2].strip()
		atb4 = row[3].strip()
		code = row[4].strip()
		desc = row[5].strip()
		if(len(atb1) + len(atb2) + len(atb3) + len(atb4) < len(desc)): return "%s - %s" % (code, desc)
		else: return "%s - %s %s %s %s" % (code, atb1, atb2, atb3, atb4)
	return 'Unknown!'

def isReturn(ticket_code):
	return 'R' in ticket_code

def printDetails(r):
	if r.type != '~FR':
		print unicode(r)
		print "Route: " + getRouteDesc(r.flow)
		res = getRestriction(r.flow, isReturn(r.type))
		if res != '': print res
	else:
		print "Two single tickets:"
		out = getDirect(r.origin, r.destination, singleTable)
		rtn = getDirect(r.destination, r.origin, singleTable)
		print "Outward:",
		printDetails(out)
		print "Return:",
		printDetails(rtn)

def getStationName(code):
	cursor = cnxn.cursor()
	cursor.execute("SELECT `Name` FROM `StationInfo` WHERE `TLC` = '%s'" % code)
	incQueryCount()
	for row in cursor:
		return row[0]
	return "Unknown station"

origin = sys.argv[2]
destination = sys.argv[3]

setupTables(sys.argv[1])
setupMinFare(destination)

direct = getDirect(origin, destination)
print "Direct:",
printDetails(direct)
print "Searcing..."
route = findRoute(origin, destination, direct.fare)
fare = 0
lflow = None
print "Best route:"
for a, b in pairwise(route):
	r = getDirect(a, b)
	if lflow is not None:
		dests = getNodeStations(lflow, r.origin)
		origins = getNodeStations(r.flow, r.origin)
		if dests is not None and origins is not None:
			points = set(dests).intersection(set(origins))
			if len(points) > 1: print "  Flow intersections: %s" % ', '.join(list(points))
	print ''
	printDetails(r)
	lflow = r.flow
	fare += r.fare
print ''
print "Total: %s" % asMoney(fare)
if direct is not None: print "Saving: %s" % asMoney(direct.fare - fare)
print "DB Queries: %i" % queryCount
