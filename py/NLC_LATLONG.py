#!/usr/bin/python

import MySQLdb
from bng_to_latlon import OSGB36toWGS84
from dbconfig import getDbWriteLogin

dbLogin = getDbWriteLogin()

def get_cnxn():
	return MySQLdb.connect(host="localhost",
                      user=dbLogin[0],
                      passwd=dbLogin[1],
                      db="djsplit")

def getOSEN(crs):
	lcnxn = get_cnxn()
	cursor = lcnxn.cursor()
	cursor.execute("SELECT OSEasting, OSNorthing FROM StationInfo WHERE TLC = '%s'" % crs)
	for row in cursor:
		return int(row[0]), int(row[1])
	return None, None
	
def nlc_exists(nlc):
	lcnxn = get_cnxn()
	excur = lcnxn.cursor()
	excur.execute("SELECT COUNT(*) FROM NLC_LATLONG WHERE NLC = '%s'" % nlc)
	for row in excur:
		if(int(row[0]) > 0): return True
	return False

def save_latlong(nlc, lat, long):
	if nlc_exists(nlc): return
	lcnxn = get_cnxn()
	inscur = lcnxn.cursor()
	inscur.execute("INSERT INTO NLC_LATLONG (NLC, LAT, `LONG`) VALUES ('%s', %f, %f)" % (nlc, lat, long))
	lcnxn.commit()

cnxn = get_cnxn()
cursor = cnxn.cursor()
cursor.execute("SELECT NLC_CODE, FARES_GROUP, CRS_CODE FROM LOCATIONS")
for row in cursor:
	nlc = row[0]
	fg = row[1]
	crs = row[2]
	print crs
	east, north = getOSEN(crs)
	if east is not None:
		lat, long = OSGB36toWGS84(east, north)
		save_latlong(nlc, lat, long)
		save_latlong(fg, lat, long)
