#!/usr/bin/python

import MySQLdb
import struct
from dbconfig import getDbWriteLogin

dbLogin = getDbWriteLogin()

cnxn = MySQLdb.connect(host="localhost",
                      user=dbLogin[0],
                      passwd=dbLogin[1],
                      db="djsplit")

					  
def getstructparser(fieldwidths):
	fmtstring = ' '.join('{}{}'.format(abs(fw), 'x' if fw < 0 else 's')
                        for fw in fieldwidths)
	return struct.Struct(fmtstring).unpack_from
	

flow_parse = getstructparser((-2, 4, 4, 5, 3, 1, 1, 8, 8, 3, 1, 1, 1, 7))
def process_flow(line):
	global flow_parse
	fields = flow_parse(line)
	from UserString import MutableString
	sql = MutableString()
	sql += 'INSERT INTO FLOWS (`ORIGIN_CODE`,`DESTINATION_CODE`,`ROUTE_CODE`,`STATUS_CODE`,`USAGE_CODE`,`DIRECTION`,`END_DATE`,`START_DATE`,`TOC`,`CROSS_LONDON_IND`,`NS_DISC_IND`,`PUBLICATION_IND`,`FLOW_ID`) VALUES '
	sql += '('
	sql += ', '.join(["'{}'".format(f) for f in fields])
	sql += ')'
	cursor = cnxn.cursor()
	cursor.execute(str(sql))
	cnxn.commit()

fare_parse = getstructparser((-2, 7, 3, 8, 2))
def process_fare(line):
	global fare_parse
	fields = fare_parse(line)
	from UserString import MutableString
	sql = MutableString()
	sql += 'INSERT INTO FARES (`FLOW_ID`,`TICKET_CODE`,`FARE`,`RESTRICTION_CODE`) VALUES '
	sql += '('
	sql += ', '.join(["'{}'".format(f) for f in fields])
	sql += ')'
	cursor = cnxn.cursor()
	cursor.execute(str(sql))
	cnxn.commit()

file = open("../data/RJFAF017.FFL")

count = 0
for line in file:
	count += 1
	print count, line
	type = line[1]
	if type == 'F':
		process_flow(line)
	if type == 'T':
		process_fare(line)
