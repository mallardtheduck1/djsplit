#!/usr/bin/python

import MySQLdb
import struct
from dbconfig import getDbWriteLogin

dbLogin = getDbWriteLogin()

cnxn = MySQLdb.connect(host="localhost",
                      user=dbLogin[0],
                      passwd=dbLogin[1],
                      db="djsplit")
					  
def getstructparser(fieldwidths):
	fmtstring = ' '.join('{}{}'.format(abs(fw), 'x' if fw < 0 else 's')
                        for fw in fieldwidths)
	return struct.Struct(fmtstring).unpack_from
	

cluster_parse = getstructparser((-1, 4, 4, 8, 8))
def process_cluster(line):
	global cluster_parse
	fields = cluster_parse(line)
	from UserString import MutableString
	sql = MutableString()
	sql += 'INSERT INTO CLUSTERS (`CLUSTER_ID`,`CLUSTER_NLC`,`END_DATE`,`START_DATE`) VALUES '
	sql += '('
	sql += ', '.join(["'{}'".format(f) for f in fields])
	sql += ')'
	cursor = cnxn.cursor()
	cursor.execute(str(sql))
	cnxn.commit()

file = open("../data/RJFAF017.FSC")

count = 0
for line in file:
	count += 1
	print count, line
	type = line[0]
	if type == 'R':
		process_cluster(line)
