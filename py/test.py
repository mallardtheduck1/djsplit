import pyodbc

cnxn = pyodbc.connect("Driver={SQL Server Native Client 11.0};"
                      "Server=.\LOCALHOST;"
                      "Database=R;"
                      "Trusted_Connection=yes;")

def tlcToNlc(tlc):
	cursor = cnxn.cursor()
	cursor.execute('SELECT FARES_GROUP, NLC_CODE FROM LOCATIONS WHERE CRS_CODE = \'' + tlc + '\'')
	ret = []
	for row in cursor:
		nlc = row[0].strip()
		fg = row[1].strip()
		if(nlc not in ret): ret.append(nlc)
		if(fg not in ret): ret.append(fg)
		ret.append(row[0])
	return ret
					  
def getLocs(tlc):
	locs = tlcToNlc(tlc)
	cursor = cnxn.cursor()
	ret = []
	for loc in locs:
		loc = loc.strip()
		if(loc not in ret):
			ret.append(loc)
			cursor.execute('SELECT CLUSTER_ID FROM CLUSTERS WHERE CLUSTER_NLC = \'' + loc + '\'')
			for row in cursor:
				nlc = row[0].strip()
				if(nlc not in ret): ret.append(nlc)
	return ret
	
def getAllFares(olocs, dlocs):
	sql = 'SELECT FLOW_ID FROM FLOWS WHERE (ORIGIN_CODE IN ('
	sql += ', '.join(["'{}'".format(o) for o in olocs])
	sql += ') AND DESTINATION_CODE IN ('
	sql += ', '.join(["'{}'".format(d) for d in dlocs])
	sql += ')) OR (DESTINATION_CODE IN ('
	sql += ', '.join(["'{}'".format(o) for o in olocs])
	sql += ') AND ORIGIN_CODE IN ('
	sql += ', '.join(["'{}'".format(d) for d in dlocs])
	sql += ') AND DIRECTION = \'R\')'
	cursor = cnxn.cursor()
	cursor.execute(sql)
	flows = []
	for row in cursor:
		flows.append(row[0])
	ret = []
	for f in flows:
		sql = 'SELECT * FROM FARES WHERE FLOW_ID = \'' + f + '\''
		cursor.execute(sql)
		for row in cursor:
			ret.append(row)
	sql = 'SELECT * FROM NDFARES WHERE ORIGIN_CODE IN ('
	sql += ', '.join(["'{}'".format(o) for o in olocs])
	sql += ') AND DESTINATION_CODE IN ('
	sql += ', '.join(["'{}'".format(d) for d in dlocs])
	sql += ')'
	cursor.execute(sql)
	for row in cursor:
			ret.append(row)
	return ret

def getFares(origin, destination):
	olocs = getLocs(origin)
	dlocs = getLocs(destination)
	fares = getAllFares(olocs, dlocs)
	
	for f in fares:
		print "{}".format(f)
		
		
getFares('WHI', 'VIC')