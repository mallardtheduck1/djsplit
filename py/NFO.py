#!/usr/bin/python

import MySQLdb
import struct
from dbconfig import getDbWriteLogin

dbLogin = getDbWriteLogin()

cnxn = MySQLdb.connect(host="localhost",
                      user=dbLogin[0],
                      passwd=dbLogin[1],
                      db="djsplit")

					  
def getstructparser(fieldwidths):
	fmtstring = ' '.join('{}{}'.format(abs(fw), 'x' if fw < 0 else 's')
                        for fw in fieldwidths)
	return struct.Struct(fmtstring).unpack_from
	

ndf_parse = getstructparser((-1, 4, 4, 5, 3, 3, 1, 8, 8, 8, 1, 8, 8, 2, 1, 1, 1))
def process_ndf(line):
	global ndf_parse
	fields = ndf_parse(line)
	from UserString import MutableString
	sql = MutableString()
	sql += 'INSERT INTO NDFARES (`ORIGIN_CODE`,`DESTINATION_CODE`,`ROUTE_CODE`,`RAILCARD_CODE`,`TICKET_CODE`,`ND_RECORD_TYPE`,`END_DATE`,`START_DATE`,`QUOTE_DATE`,`SURPRESS_MKR`,`ADULT_FARE`,`CHILD_FARE`,`RESTRICTION_CODE`,`COMPOSITE_INDICATOR`,`CROSS_LONDON_IND`,`PS_IND`) VALUES '
	sql += '('
	sql += ', '.join(["'{}'".format(f) for f in fields])
	sql += ')'
	cursor = cnxn.cursor()
	cursor.execute(str(sql))
	cnxn.commit()

file = open("../data/RJFAF017.NFO")

count = 0
for line in file:
	count += 1
	print count, line
	type = line[0]
	if type == 'R':
		process_ndf(line)
