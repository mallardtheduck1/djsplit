#!/usr/bin/python

import MySQLdb
import struct
from dbconfig import getDbWriteLogin

dbLogin = getDbWriteLogin()

cnxn = MySQLdb.connect(host="localhost",
                      user=dbLogin[0],
                      passwd=dbLogin[1],
                      db="djsplit")
					  
def getstructparser(fieldwidths):
	fmtstring = ' '.join('{}{}'.format(abs(fw), 'x' if fw < 0 else 's')
                        for fw in fieldwidths)
	return struct.Struct(fmtstring).unpack_from
	

route_parse = getstructparser((-2, 5, 8, 8, 8, 16, 35, 35, 35, 35, 16, 41, -19))
def process_route(line):
	global route_parse
	fields = route_parse(line)
	from UserString import MutableString
	sql = MutableString()
	sql += 'INSERT INTO ROUTES (`ROUTE_CODE`, `END_DATE`, `START_DATE`, `QUOTE_DATE`, `DESCRIPTION`, `ATB_DESC_1`, `ATB_DESC_2`, `ATB_DESC_3`, `ATB_DESC_4`, `CC_DESC`, `AAA_DESC`) VALUES '
	sql += '('
	sql += ', '.join(["'{}'".format(f.replace("'", "''")) for f in fields])
	sql += ')'
	cursor = cnxn.cursor()
	cursor.execute(str(sql))
	cnxn.commit()

file = open("../data/RJFAF017.RTE")

count = 0
for line in file:
	count += 1
	print count, line
	type = line[:2]
	if type == 'RR':
		process_route(line)
