'use strict';

class SharePanel extends React.Component {
	constructor(props) {
		super(props);
		this.state = {expand: false};
	}
	
	expand(){
		var newExpand = !this.state.expand;
		this.setState({expand: newExpand});
	}
	
	copyLink(){
		this.textArea.select();
		document.execCommand("copy");
		return false;
	}
	
	render(){
		if(this.state.expand){
			var url = window.location.href;
			return <div className="share-panel bg-info">
				<div className="share-link">
					<form>
						<label htmlFor="results_link" className="share-link-expand" onClick={this.expand.bind(this)}>
							<span className="glyphicon glyphicon-link" aria-hidden="true"></span> Link to these results
							<span className="glyphicon glyphicon-triangle-bottom text-muted" aria-hidden="true"></span>
						</label>
						<textarea ref={(textarea) => this.textArea = textarea} className="form-control share-url-box" value={url} readOnly={true} />
						<button className="btn btn-default" type="button" onClick={this.copyLink.bind(this)}>
							<span className="glyphicon glyphicon-copy" aria-hidden="true"></span>
							&nbsp;Copy to clipboard
						</button>
					</form>
				</div>
			</div>
		}else{
			return <div className="share-panel bg-info">
				<span className="share-link-expand" onClick={this.expand.bind(this)}>
					<span className="glyphicon glyphicon-link" aria-hidden="true"></span> Link to these results
					<span className="glyphicon glyphicon-triangle-right text-muted" aria-hidden="true"></span>
				</span>
			</div>
		}
	}
}