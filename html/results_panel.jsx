'use strict';

class ResultsPanel extends React.Component {
	constructor(props) {
		super(props);
	}
	
	makeTicket(details, i){
		if(details['Type'] != '~FR'){
			return <TicketDisplay 
				key = {'ticket_' + i}
				origin = {details['Origin']}
				destination = {details['Destination']}
				type = {details['Type']}
				fare = {details['Fare']}
				route = {details['Route']}
				restriction = {details['Restriction']}
			/>
		}else{
			return <div className='fr-details' key={'fr_details' + i}>
				<h3>Two single tickets:</h3>
					{this.makeTicket(details['FROut'], i + '_out')}
					{this.makeTicket(details['FRRtn'], i + '_rtn')}
				</div>
		}
	}
	
	render(){
		var that = this;
		var directFare = this.props.direct ? this.props.direct['Fare'] : null;
		var bestFare = this.props.best ? this.props.best.reduce(function(acc, cur){	return acc + cur['Fare'];}, 0) : null;
		return <div className="results-panel panel panel-default">
			<SharePanel />
			<h2>Cheapest direct ticket: {this.props.direct ? asMoney(directFare) : null}</h2>
			{this.props.direct ? this.makeTicket(this.props.direct, 'direct') : null}
		
			{ (this.props.best && this.props.best.length > 0) ?
				<div>
					<h2>Cheapest split-ticketing option found: {this.props.best ? asMoney(bestFare) : null}</h2>
					{(directFare && bestFare) ?
						<h3>Saving: {asMoney(directFare-bestFare)}</h3> : null
					}
					<div>
						{ this.props.best.map((ticket, i) => that.makeTicket(ticket, i)) }
					</div>
				</div> 
			: <h2>No better option found.</h2>}
		</div>
	}
}