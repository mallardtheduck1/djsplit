'use strict';

class TicketDisplay extends React.Component {
	constructor(props) {
		super(props);
	}
	
	stationName(code){
		var name = Object.keys(allStations).find(key => allStations[key] === code);
		return name + " (" + code + ")";
	}
	
	isReturnTicket(type){
		return type.includes('R');
	}
	
	render(){
		return <div className="ticket-display">
			<div className="ticket-body">
				<div className="ticket-left">
					<div className="ticket-row">
						<span className="ticket-label">From:</span><span className="ticket-place">
							{this.stationName(this.props.origin)}
						</span>
					</div>
					<div className="ticket-row">
						<span className="ticket-label">To:</span><span className="ticket-place">
							{this.stationName(this.props.destination)}
						</span>
					</div>
				</div>
				<div className="ticket-right">
					<div className="ticket-row">
						<span className="ticket-fare">{asMoney(this.props.fare)}</span>
					</div>
					<div className="ticket-row">
						<span className="ticket-type">{ticketTypes[this.props.type] + " (" + this.props.type+ ")"}</span>
					</div>
				</div>
				<div className="ticket-bottom">
					<div className="ticket-row">
						<span className="ticket-label">Route:</span><span className="ticket-route">{this.props.route}</span>
					</div>
					<div className="ticket-row ticket-restrict-row">
						{this.props.restriction && this.props.restriction['RestrictCode'] != null &&
							<span>
								<span className="ticket-label">Restriction:</span>
								<a href={"http://nationalrail.co.uk/" + this.props.restriction['RestrictCode']} target="_blank">
									<span className="ticket-restrict-desc">{this.props.restriction['Description']} ({this.props.restriction['RestrictCode']})</span>
									<br />
									<span className="ticket-restrict-label">Out:</span>
									<span className="ticket-restrict-text">{this.props.restriction['DescOut']}</span>
									{this.isReturnTicket(this.props.type) &&
										<span>
											<br />
											<span className="ticket-restrict-label">Rtn:</span>
											<span className="ticket-restrict-text">{this.props.restriction['DescRtn']}</span>
										</span>
									}
								</a>
							</span>
						}
					</div>
				</div>
			</div>
		</div>
	}
}