'use strict';

class SearchPanel extends React.Component {
  constructor(props) {
    super(props);
    
    this.version = 3;
    
    try{
    	var hashState = JSON.parse(window.atob(window.location.hash.substr(1)));
    }catch(e){
    	var hashState = null;
    }
    if(hashState && hashState.v && hashState.v == this.version){
    	this.state = {
    		fareSet: hashState.s, 
    		origin: hashState.o, 
    		destination: hashState.d, 
    		mode: 'retrieve',
    		route: hashState.r
    	};
    }else{
    	this.state = {fareSet: 'ST', origin: '', destination: '', mode: 'ready'};
    }
  }
  
  handleOriginChange(newOrigin){
  	this.setState({mode: 'ready', origin: newOrigin});
  }
  
  handleDestinationChange(newDestination){
  	this.setState({mode: 'ready', destination: newDestination});
  }
  
  handleFareSetChange(newFareSet){
  	this.setState({mode: 'ready', fareSet: newFareSet});
  }
  
  searchClick(){
  	this.setState({'mode': 'progress'});
  	if(this.state.origin && this.state.destination){
	  	var search = performSearch(this.state.fareSet, this.state.origin, this.state.destination);
	  	var that = this;
	  	search.then(
	  		(result) => that.setState({'mode': 'results', 'result': result})
		).catch((e) => {
	  		console.log(e);
	  		that.setState({'mode': 'error'});
	  	});
  	}
  	return false;
  }

  retrieveResults() {
  	var that = this;
  	var search = retrieveSearch(this.state.fareSet, this.state.origin, this.state.destination, this.state.route);
  	search.then(
  		(result) =>	that.setState({'mode': 'results', 'result': result})
  	).catch((e) => {
	  	console.log(e);
	  	that.setState({'mode': 'error'});
	});
  }

  render() {
  	if(this.state.mode == 'results'){
  		window.location.hash = window.btoa(JSON.stringify({
  			o: this.state.origin,
  			d: this.state.destination,
  			s: this.state.fareSet,
  			r: this.state.result.Route,
  			v: this.version
  		}));
  	}else if(this.state.mode != 'retrieve'){
  		if(window.location.hash != '') window.location.hash = '';
  	}
  	
  	if(this.state.mode == 'retrieve') this.retrieveResults();
  	
    return <div>
    	<div className="container-fluid panel panel-default">
	    	<form>
		    	<div className="row">
		    		<div className="col-md-2">
		    		</div>
			    	<div className="col-md-4">
			    		<div className="form-group">
			    			<label htmlFor="originStation">Origin</label>
			    			<StationSelector id="originStation" value={this.state.origin}
			    			onChange={this.handleOriginChange.bind(this)}/>
			    		</div>
			    	</div>
			    	<div className="col-md-4">
			    		<div className="form-group">
			    			<label htmlFor="destinationStation">Destination</label>
			    			<StationSelector id="destinationStation" value={this.state.destination}
			    			onChange={this.handleDestinationChange.bind(this)}/>
			    		</div>
			    	</div>
			    	<div className="col-md-2">
		    		</div>
			    </div>
			    <div className="row">
		    		<div className="col-md-2">
		    		</div>
			    	<div className="col-md-6">
			    		<div className="form-group">
			    			<label htmlFor="fareSet">Fare set</label>
			    			<FareSetSelector id="fareSet" value={this.state.fareSet}
			    			onChange={this.handleFareSetChange.bind(this)}/>
			    		</div>
			    	</div>
			    	<div className="col-md-2">
				    	<div className="form-group text-right">
				    		<br className="btn-spacer" />
				    		<button id="searchBtn" className="btn btn-primary btn-lg" type="button" onClick={this.searchClick.bind(this)}
				    			disabled={this.state.mode == 'progress'}
				    		>
				    			<span className="glyphicon glyphicon-search" aria-hidden="true"></span>
					    		&nbsp;Search
				    		</button>
			    		</div>
		    		</div>
		    		<div className="col-md-2">
		    		</div>
	    		</div>
	    	</form>
    	</div>
    	{(this.state.mode == 'progress' || this.state.mode == 'retrieve') &&
    	<ProgressPanel />
    	}
    	{this.state.mode == 'results' &&
    	<ResultsPanel 
    		direct={this.state.result ? this.state.result['Direct'] : null} 
    		best={this.state.result ? this.state.result['Best'] : null} 
    	/>}
    	{this.state.mode == 'error' &&
    		<div className="panel panel-default">
    			<h1>Ooops! An error occured while searching.</h1>
    			<p>Try <a href=".">refreshing the page</a> and searching again.</p>
    		</div>
    	}
    </div>
  }
}

const domContainer = document.querySelector('#search_panel');
ReactDOM.render(<SearchPanel/>, domContainer);