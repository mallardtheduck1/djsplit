<?php

exec("make 2>&1", $output, $return_var);
$result = implode("\n", $output);

$made = (strpos($result, 'Nothing') === false);

if($made){
	if($return_var == 0){
		echo "window.location.reload();";
	}else{
		if($_COOKIE["djsplit_developer"] === "secretly_enabled"){
			echo "console.error(" . json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE)  . ");";
		}else{
			echo "console.error(\"A server-side compilation error occured.\");";
		}
	}
}
