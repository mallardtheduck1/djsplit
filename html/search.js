async function callApi(data){
	var result = await $.ajax({
		'url': 'api.php',
		'data': data
	});
	return $.parseJSON(result);
}

function getMinFares(set, dest){
	var result = callApi({
		'function': 'getMinFares',
		'set': set,
		'destination': dest
	});
	return result;
}

function getEdges(set, node, maxFare){
	var result = callApi({
		'function': 'getEdges',
		'set': set,
		'node': node,
		'maxFare': maxFare
	});
	return result;
}

function getDirect(set, origin, destination, single){
	var result = callApi({
		'function': 'getDirect',
		'set': set,
		'origin': origin,
		'destination': destination,
		'single': single
	});
	return result;
}

function getNodeStations(flow, station){
	var result = callApi({
		'function': 'getNodeStations',
		'flow': flow,
		'station': station
	});
	return result;
}

function getRestriction(set, origin, destination, single){
	var result = callApi({
		'function': 'getRestriction',
		'set': set,
		'origin': origin,
		'destination': destination,
		'single': single
	});
	return result;
}

function getRouteDesc(flow){
	var result = callApi({
		'function': 'getRouteDesc',
		'flow': flow
	});
	return result;
}

function guesstimateFare(minFares, origin){
	var result = minFares[origin];
	if(!result) return 0;
	return result;
}

async function findRoute(set, origin, destination, directFare){
	var frontier = new PriorityQueue((a, b) => a[0] < b[0]);
	frontier.push([0, origin]);
	var came_from = {};
	var cost_so_far = {};
	came_from[origin] = null;
	cost_so_far[origin] = 0;
	visited = [];
	var minFound = directFare;
	var minFares = await getMinFares(set, destination);
	
	while(!frontier.isEmpty()){
		cur = frontier.pop();
		current = cur[1];
		if(visited.includes(current)) continue;
		if(cost_so_far[current] > minFound) continue;
		
		if(current == destination){
			route = [];
			while(Object.keys(came_from).includes(current)){
				route.push(current);
				current = came_from[current];
			}
			route.reverse();
			console.log(route);
			return route;
		}
		
		var costRemaining = minFound - cost_so_far[current];
		var edges = await getEdges(set, current, costRemaining);
		edges.forEach(function(edge){
			next = edge['D'];
			fare = edge['F'];
			if(fare > costRemaining) return;
			var guessNext = guesstimateFare(minFares, next) + fare;
			if(guessNext > costRemaining) return;
			var new_cost = cost_so_far[current] + fare;
			if(next == destination && new_cost < minFound) minFound = new_cost;
			if(!Object.keys(cost_so_far).includes(next) || new_cost < cost_so_far[next]){
				cost_so_far[next] = new_cost;
				var guess = guesstimateFare(minFares, next);
				var priority = new_cost + guess;
				frontier.push([priority, next]);
				came_from[next] = current;
			}
		});
		visited.push(current);
	}
	console.log("No better route found.");
	return [];
}

async function fetchTicketDetails(set, origin, destination, single){
	var basic = await getDirect(set, origin, destination, single);
	var route = await getRouteDesc(basic['FlowID']);
	var restrict = await getRestriction(set, origin, destination, single);
	return {
		'Origin': basic['Origin'],
		'Destination': basic['Destination'],
		'Fare': basic['Fare'],
		'Type': basic['Type'],
		'Route': route,
		'Restriction': restrict
	};
}

async function getTicketDetails(set, origin, destination){
	var ticket = await fetchTicketDetails(set, origin, destination);
	if(ticket['Type']=='~FR'){
		ticket['FROut'] = await fetchTicketDetails(set, origin, destination, true);
		ticket['FRRtn'] = await fetchTicketDetails(set, destination, origin, true);
	}
	return ticket;
}

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

async function getRouteDetails(set, origin, destination, route){
	details = [];
	await asyncForEach(route, async function(r, i){
		var next = route[i + 1];
		if(next != null){
			var ticket = await getTicketDetails(set, r, next);
			details.push(ticket);
		}
	});
	
	return {
		'Route': route,
		'Best': details
	};
}

async function performSearch(set, origin, destination){
	var direct = await getDirect(set, origin, destination, false);
	route = await findRoute(set, origin, destination, direct['Fare']);
	var directTicket = await getTicketDetails(set, origin, destination);
	
	var ret = await getRouteDetails(set, origin, destination, route);
	ret['Direct'] = directTicket;
	return ret;
}

async function retrieveSearch(set, origin, destination, route){
	var direct = await getDirect(set, origin, destination, false);
	var directTicket = await getTicketDetails(set, origin, destination);
	
	var ret = await getRouteDetails(set, origin, destination, route);
	ret['Direct'] = directTicket;
	return ret;
}

function asMoney(value){
	value = "" + value;
	if(value.length < 3) return value + 'p';
	else{
		pence = value.slice(-2);
		pounds = value.slice(0, -2);
		return "\u00A3" + pounds + "." + pence;
	}
}
	