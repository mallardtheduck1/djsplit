<?php

require_once('db_password.php');

function getDb(){
	$dbLogin = getDbLogin();
	return new mysqli("localhost", $dbLogin[0], $dbLogin[1], "djsplit");
}

function getSet($set){
	switch($set){
		case 'ST':
			return array(
				'FareGraph' => 'FareGraphST',
				'MinFares' => 'MinFaresST',
			);
		case 'SU':
			return array(
				'FareGraph' => 'FareGraphSU',
				'MinFares' => 'MinFaresSU',
			);
		case 'RT':
			return array(
				'FareGraph' => 'FareGraphRT',
				'MinFares' => 'MinFaresRT',
				'SingleFares' => 'FareGraphST',
			);
		case 'RU':
			return array(
				'FareGraph' => 'FareGraphRU',
				'MinFares' => 'MinFaresRU',
				'SingleFares' => 'FareGraphSU',
			);
	}
}

function getMinFares($set, $dest){
	$db = getDb();
	$config = getSet($set);
	$mftbl = $config['MinFares'];
	$query = $db->prepare("SELECT MinFare FROM $mftbl WHERE Destination = ?");
	$query->bind_param('s', $dest);
	$query->execute();
	$query->bind_result($mf);
	$query->fetch();
	$query->close();
	
	$fgtbl = $config['FareGraph'];
	$query = $db->prepare("SELECT fg.Origin, fd.DISTANCE * ? AS Min FROM $fgtbl fg " .
		"INNER JOIN FLOW_DISTANCE fd ON fg.FlowID = fd.FLOW_ID " .
		"WHERE fg.Destination = ?");
	$query->bind_param('ds', $mf, $dest);
	$query->execute();
	$result = $query->get_result();
	$ret = array();
	while($row = $result->fetch_assoc()){
		$ret[$row['Origin']] = $row['Min'];
	}
	return $ret;
}

function getEdges($set, $node, $maxFare){
	$db = getDb();
	$config = getSet($set);
	$fgtbl = $config['FareGraph'];
	$query = $db->prepare("SELECT Destination, Fare FROM $fgtbl WHERE Origin = ? AND Fare < ?");
	$query->bind_param('si', $node, $maxFare);
	$query->execute();
	$result = $query->get_result();
	$ret = array();
	while($row = $result->fetch_assoc()){
		$ret[] = array(
			'D' => $row['Destination'],
			'F' => $row['Fare'],
		);
	}
	return $ret;
}

function getDirect($set, $origin, $dest, $single){
	$db = getDb();
	$config = getSet($set);
	if($single == 'true') $fgtbl = $config['SingleFares'];
	else $fgtbl = $config['FareGraph'];
	$query = $db->prepare("SELECT Origin, Destination, Fare, FlowID, Type FROM $fgtbl WHERE Origin = ? AND Destination = ?");
	$query->bind_param('ss', $origin, $dest);
	$query->execute();
	$query->bind_result($origin, $destination, $fare, $flowId, $type);
	$query->fetch();
	$query->close();
	return array(
			'Origin' => $origin,
			'Destination' => $destination,
			'Fare' => $fare,
			'FlowID' => $flowId,
			'Type' => $type,
	);
}

function nlcToTlcs($db, $nlc){
	$ret = array();
	$query = $db->prepare("SELECT TLC FROM StationInfo WHERE NLC = ?");
	$query->bind_param('s', $nlc);
	$query->execute();
	$result = $query->get_result();
	while($row = $result->fetch_assoc()){
		$ret[] = trim($row['TLC']);
	}
	$query->close();
	
	$query = $db->prepare("SELECT si.TLC FROM CLUSTERS cl INNER JOIN StationInfo si ON cl.CLUSTER_NLC = si.NLC WHERE CLUSTER_ID = ?");
	$query->bind_param('s', $nlc);
	$query->execute();
	$result = $query->get_result();
	while($row = $result->fetch_assoc()){
		$ret[] = trim($row['TLC']);
	}
	$query->close();
	
	$query = $db->prepare("SELECT CRS_CODE FROM LOCATIONS WHERE FARES_GROUP = ?");
	$query->bind_param('s', $nlc);
	$query->execute();
	$result = $query->get_result();
	while($row = $result->fetch_assoc()){
		$ret[] = trim($row['CRS_CODE']);
	}
	$query->close();
	return $ret;
}

function getOriginStations($db, $flow){
	$ret = array();
	$query = $db->prepare("SELECT ORIGIN_CODE FROM FLOWS WHERE FLOW_ID = ?");
	$query->bind_param('s', $flow);
	$query->execute();
	$query->bind_result($nlc);
	$query->fetch();
	$query->close();
	
	return nlcToTlcs($db, $nlc);
}

function getDestinationStations($db, $flow){
	$ret = array();
	$query = $db->prepare("SELECT DESTINATION_CODE FROM FLOWS WHERE FLOW_ID = ?");
	$query->bind_param('s', $flow);
	$query->execute();
	$query->bind_result($nlc);
	$query->fetch();
	$query->close();
	
	return nlcToTlcs($db, $nlc);
}

function getNodeStations($flow, $station){
	$db = getDb();
	$os = getOriginStations($db, $flow);
	if(in_array($station, $os)) return $os;
	$ds = getDestinationStations($db, $flow);
	if(in_array($station, $ds)) return $ds;
	return array();
}

function getRestriction($set, $origin, $destination, $single){
	$db = getDb();
	$config = getSet($set);
	if($single == 'true') $fgtbl = $config['SingleFares'];
	else $fgtbl = $config['FareGraph'];
	$query = $db->prepare("SELECT `DESCRIPTION`, `DESC_OUT`, `DESC_RTN`, R.`RESTRICTION_CODE` FROM `RESTRICTIONS` R " .
		"INNER JOIN `FARES` F ON F.`RESTRICTION_CODE` = R.`RESTRICTION_CODE` " .
		"INNER JOIN $fgtbl FG ON F.`FLOW_ID` = FG.`FlowID` AND F.`TICKET_CODE` = FG.`Type` " .
		"WHERE FG.`Origin` = ? AND FG.`Destination` = ?");
	$query->bind_param('ss', $origin, $destination);
	$query->execute();
	$query->bind_result($description, $desc_out, $desc_rtn, $restrict_code);
	$query->fetch();
	$query->close();
	return array(
		'RestrictCode' => $restrict_code,
		'Description' => trim($description),
		'DescOut' => trim($desc_out),
		'DescRtn' => trim($desc_rtn),
	);
}

function getRouteDesc($flow){
	$db = getDb();
	$query = $db->prepare("SELECT R.`ROUTE_CODE`, R.`DESCRIPTION` FROM `ROUTES` R " .
		"INNER JOIN `FLOWS` F ON F.`ROUTE_CODE` = R.`ROUTE_CODE` " .
		"WHERE F.`FLOW_ID` = ?");
	$query->bind_param('s', $flow);
	$query->execute();
	$query->bind_result($code, $desc);
	$query->fetch();
	$query->close();
	return "$code - $desc";
}