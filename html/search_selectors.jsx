'use strict';

class StationSelector extends React.Component {
	constructor(props) {
		super(props);
	}
	
	generateOptions(){
		var ret = Object.keys(allStations).map((e, i) => {
			return <option key={"station_" + i} value={allStations[e]}>{e + " (" + allStations[e] + ")"}</option>
		});
		return ret;
	}
	
	changed(e){
		this.setState({selected: e.currentTarget.value});
		this.props.onChange(e.currentTarget.value);
	}
	
	render() {
		return <select id={this.props.id} className="form-control" onChange={this.changed.bind(this)} value={this.props.value}>
			<option value="" disabled={true}>Please select a station</option>
			{this.generateOptions()}
		</select>
	}
}

class FareSetSelector extends React.Component {
	constructor(props) {
		super(props);
	}
	
	changed(e){
		this.props.onChange(e.currentTarget.value);
	}
	
	render() {
		return <select id={this.props.id} className="form-control"  onChange={this.changed.bind(this)} value={this.props.value}>
			<option value="ST">Single fares (Anytime/Off-Peak only)</option>
			<option value="SU">Single fares (Anytime/Off-Peak/Super-Off-Peak)</option>
			<option value="RT">Period return fares (Anytime/Off-Peak only)</option>
			<option value="RU">Period return fares (Anytime/Off-Peak/Super-Off-Peak)</option>
		</select>
	}
}