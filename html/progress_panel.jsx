'use strict';

class ProgressPanel extends React.Component {
	constructor(props) {
		super(props);
	}
	
	render(){
		return <div className="panel panel-default">
			<h1>Searching for the cheapest fares...</h1>
			<div className="train-spinner">
				<div className="cloud anim-accross">
				</div>
				<div className="cloud anim-accross cloud2">
				</div>
				<div className="hill anim-accross">
				</div>
				<img className="train anim-librate" src="train.svg" />
				<div className="ground">
					<div className="track">
					</div>
					<div className="sleepers anim-accross">
					</div>
				</div>
				<div className="tree anim-accross">
					<div className="trunk">
					</div>
					<div className="canopy">
					</div>
				</div>
				<div className="tree anim-accross tree2">
					<div className="trunk">
					</div>
					<div className="canopy">
					</div>
				</div>
			</div>
			<p className="centred-text">
				Taking a while? Longer-distance and more expensive fares can be slow to find.
			</p>
		</div>
	}
}