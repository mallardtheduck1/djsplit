<?php

require_once('data.php');

$function = $_GET['function'];
$set = $_GET['set'];

switch($function){
	case 'getMinFares':
		$dest = $_GET['destination'];
		$ret = getMinFares($set, $dest);
		break;
	case 'getEdges':
		$node = $_GET['node'];
		$maxFare = $_GET['maxFare'];
		$ret = getEdges($set, $node, $maxFare);
		break;
	case 'getDirect':
		$origin = $_GET['origin'];
		$dest = $_GET['destination'];
		$single = $_GET['single'];
		$ret = getDirect($set, $origin, $dest, $single);
		break;
	case 'getNodeStations':
		$flow = $_GET['flow'];
		$station = $_GET['station'];
		$ret = getNodeStations($flow, $station);
		break;
	case 'getRestriction':
		$origin = $_GET['origin'];
		$dest = $_GET['destination'];
		$single = $_GET['single'];
		$ret = getRestriction($set, $origin, $dest, $single);
		break;
	case 'getRouteDesc':
		$flow = $_GET['flow'];
		$ret = getRouteDesc($flow);
		break;
}

echo json_encode($ret);